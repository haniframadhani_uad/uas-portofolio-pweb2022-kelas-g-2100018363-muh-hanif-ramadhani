const hamburger = document.querySelector('.hamburger');
const navLinks = document.querySelector('.nav-links');
const btn = document.querySelector('.btn');
hamburger.addEventListener('click', () => {
    hamburger.classList.toggle('active');
    navLinks.classList.toggle('active');
});

const ename = document.querySelector('.error-name');
const eemaile = document.querySelector('.error-email-empty');
const eemaili = document.querySelector('.error-email-invalid');
const emessage = document.querySelector('.error-message');
const inputName = document.getElementById('name');
const inputEmail = document.getElementById('email');
const inputMessage = document.getElementById('message');

btn.addEventListener("click", e => {
    if (inputName.value == '') {
        ename.classList.add("error");
        e.preventDefault();
    } else {
        ename.classList.remove("error");
    }
    if (inputEmail.value == '') {
        eemaile.classList.add("error");
        eemaili.classList.remove("error");
        e.preventDefault();
    } else {
        if (validateEmail(inputEmail.value) == true) {
            eemaili.classList.remove("error");
            eemaile.classList.remove("error");
        } else {
            eemaili.classList.add("error");
            eemaile.classList.remove("error");
            e.preventDefault();
        }
    }
    if (inputMessage.value == '') {
        emessage.classList.add("error");
        e.preventDefault();
    } else {
        emessage.classList.remove("error");
    }
});

function validateEmail(elementValue) {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
} 