<?php
$nav = ["home", "about", "project", "skill", "contact"];

$project = [["image" => "project1.png", "title" => "Coding skills are in demand, but companies want more from technology professionals", "desc" => "The ascendance of digital enterprises -- powered by AI, machine learning, and cloud-based services -- is recasting the career opportunities of technology managers and professionals. Coding skills continue to be in demand, but companies ultimately want more, and as a result, IT roles are being pushed upward. "], ["image" => "project2.webp", "title" => "Coding Isn’t a Necessary Leadership Skill — But Digital Literacy Is", "desc" => "When former Microsoft U.S. chief technology officer Jennifer Byrne was offered her role, she worried that she didn’t know enough about technology. After all, Microsoft’s suite of tech products is just so vast."], ["image" => "project3.webp", "title" => "First she taught herself how to code, then she founded a coding school in Penang for kids", "desc" => "If you were to Google the benefits of coding, you will come across tons of benefits as to why you should pursue a career in coding. For instance, coding is said to boost problem-solving and logical thinking skills, and improve interpersonal skills."], ["image" => "project4.webp", "title" => "This popular laptop backpack is perfect for school and travel — but it’s selling out fast", "desc" => "Our team is dedicated to finding and telling you more about the products and deals we love. If you love them too and decide to purchase through the links below, we may receive a commission. Pricing and availability are subject to change."]];
$skill = [["image" => "Web-Design.svg", "alt" => "Web-Design", "skill" => "Web Design"], ["image" => "3D-Model.svg", "alt" => "3D-Model", "skill" => "3d Modeling"], ["image" => "Photo-Product.svg", "alt" => "Photo-Product", "skill" => "Photo Product"], ["image" => "Motion-Graphic.svg", "alt" => "Motion-Graphic", "skill" => "Motion Graphic"]];

$social = [["link" => "https://www.instagram.com/", "name" => "instagram"], ["link" => "https://facebook.com/", "name" => "facebook"], ["link" => "https://www.youtube.com/", "name" => "youtube"], ["link" => "https://twitter.com/", "name" => "twitter"], ["link" => "https://gitlab.com/", "name" => "gitlab"], ["link" => "https://github.com/", "name" => "github"], ["link" => "https://bitbucket.com/", "name" => "bitbucket"], ["link" => "https://codepen.io/", "name" => "codepen"], ["link" => "https://freecodecamp.org/", "name" => "freecodecamp"], ["link" => "https://sololearn.com/", "name" => "sololearn"], ["link" => "https://coursera.org/", "name" => "coursera"], ["link" => "https://codewars.com/", "name" => "codewars"], ["link" => "https://linkedin.com/", "name" => "linkedin"]];

$eEmail = false;
$eName = false;
$eMessage = false;
function sendmail($data)
{
	if (!empty($data["email"])) {
		$fromEmail = htmlspecialchars($data["email"]);
		$fromEmail = "From: " . $fromEmail;
	} else {
		return $eEmail = true;
	}
	$toEmail = "emailsaya@gmail.com";
	if (!empty($data["name"])) {
		$subject = htmlspecialchars($data["name"]);
		$subject = "From: " . $subject;
	} else {
		return $eName = true;
	}
	if (!empty($data["message"])) {
		$message = htmlspecialchars($data["message"]);
	} else {
		return $eMessage = true;
	}

	return mail($toEmail, $subject, $message, $fromEmail);
}